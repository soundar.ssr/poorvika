const OrderDetails = ({ order }) => {
  return (
    <div className="card row m-2">
      <div className="col-md-12 cart">
        <div className="title">
          <div className="row">
            <div className="col m-2">
              <h4>
                <b>Payment Details</b>
              </h4>
            </div>
          </div>
        </div>

        <div className="row border-top border-bottom">
          <div className="row main align-items-center m-2">
            <div>
              <strong>Name : </strong>
              {order.payment_details.payment_firstname}
            </div>
            <div>
              <strong>Phone : </strong>
              {order.payment_details.payment_mobile}
            </div>
          </div>
          <div className="row main align-items-center m-2">
            <div>
              <strong>Billing Address </strong>
            </div>
            <div>{order.payment_details.payment_company}</div>
            <div>
              {order.payment_details.payment_address_1},{" "}
              {order.payment_details.payment_address_2}
            </div>
            <div>{order.payment_details.payment_area}</div>
            <div>{order.payment_details.payment_city}</div>
            <div>
              {order.payment_details.payment_country} -{" "}
              {order.payment_details.payment_postcode}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrderDetails;
