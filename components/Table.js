import React from "react";
import MUIDataTable from "mui-datatables";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import {useRouter} from 'next/router'


const ExpandableRowTable = props => {
  const {columns, data} = props;
  const router = useRouter()


  const handleRowClick = (id) => {
    router.push('/orders/'+id)
};

function getOrderStatus(status) {
  if (status === "Open") {
    return <span className="open status">Open</span>;
  } else if (status === "Pending") {
    return <span className="pending status">Pending</span>;
  }
  if (status === "Closed") {
    return <span className="closed status">Closed</span>;
  }
}

function getOrderCurrent(status) {
  if (status === "New") {
    return <span className="status new">New</span>;
  } else {
    return <span className="status other-status ">{status}</span>;
  }
}


  function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
  }

  const rows = [
    createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
    createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
    createData("Eclair", 262, 16.0, 24, 6.0),
    createData("Cupcake", 305, 3.7, 67, 4.3),
    createData("Gingerbread", 356, 16.0, 49, 3.9)
  ];

  const options = {
    filter: true,
    onFilterChange: (changedColumn, filterList) => {
      console.log(changedColumn, filterList);
    },
    selectableRows: "single",
    filterType: "dropdown",
    responsive: "standard",
    rowsPerPage: 10,
    expandableRows: true,
    // eslint-disable-next-line react/display-name
    renderExpandableRow: (rowData, rowMeta) => {
      console.log(rowData)
      return (
        <React.Fragment>
          <tr>
            <td colSpan={6}>
              <TableContainer component={Paper}>
                <Table style={{ minWidth: "650" }} aria-label="simple table">
                  <TableHead>
                   <TableRow>
                    <TableCell>Order ID</TableCell>
                    <TableCell>Order Status</TableCell>
                    <TableCell>Total Products</TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell>Current Status</TableCell>
                    <TableCell>Modified</TableCell>
                  </TableRow>
                  </TableHead>
                  <TableBody>
                    {rowData[3].map(row => (
                      <TableRow style={{cursor:'pointer'}} onClick={()=>handleRowClick(row.order_id)} key={row.name}>
                         <TableCell component="th" scope="row">
                          {row.order_id}
                        </TableCell>
                        <TableCell component="th" scope="row">
                          {row.order_status}
                        </TableCell>
                        <TableCell component="th" scope="row">
                          {row.product_details.length}
                        </TableCell>
                        <TableCell component="th" scope="row">
                          {getOrderStatus(row.status)}
                        </TableCell>
                        <TableCell component="th" scope="row">
                          {getOrderCurrent(row.current_status)}
                        </TableCell>
                        <TableCell component="th" scope="row">
                          {row.date_added}
                        </TableCell>
                        
                        
                      </TableRow>
                       ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </td>
          </tr>
        
        </React.Fragment>
      );
    },
    page: 1
  };

  return (
    <MUIDataTable
      title={"Missed Order list"}
      data={data}
      columns={columns}
      options={options}
      
    />
  );
};

export default ExpandableRowTable;
