import Image from "next/image";

const OrderDetails = ({ order }) => {
  
  return (
    <div className="card row m-2">
      <div className="col-md-12 cart">
        <div className="title">
          <div className="row">
            <div className="col m-2">
              <h4>
                <b>Product Details</b>
              </h4>
            </div>
            <div className="col align-self-center text-right text-muted">
              {order.product_details.length} items
            </div>
          </div>
        </div>
        {order.product_details.map((product) => {
          return (
            <div
              className="row border-top border-bottom"
              key={product.order_product_id}
            >
              <div className="row main align-items-center m-2">
                <div className="col-2">
                  <Image width="50" height="100" src="/main.jpg" alt="" />
                </div>
                <div className="col">
                  <div className="row text-muted">{product.product_name}</div>
                  <div className="row">{product.model}</div>
                  <div className="row">{product.item_code}</div>
                </div>
                <div className="col"> {product.quantity} </div>
                <div className="col">Rs. {product.price} </div>
              </div>
            </div>
          );
        })}
        <div className="row">
          <strong style={{margin:"10px 0px"}}>Total Amount : {order.total}</strong>
          <strong style={{margin:"10px 0px"}}>Order Status : <span style={{color:'red'}}>{order.order_status}</span></strong>
        </div>
      </div>
    </div>
  );
};

export default OrderDetails;
