import {useRouter} from 'next/router'
import MUIDataTable from "mui-datatables";
import { createTheme, MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import {
    LocalShipping as StatusIcon,
    BatteryFull as Battery,
    SignalWifi0Bar as Gsm0,
    SignalWifi1Bar as Gsm1,
    SignalWifi2Bar as Gsm2,
    SignalWifi3Bar as Gsm3,
    SignalWifi4Bar as GsmFull,
} from "@material-ui/icons";


const getMuiTheme = () => createTheme({
    overrides: {
      MUIDataTable: {
        root: {
        },
        paper: {
          boxShadow: "none",
        }
      },
      MUIDataTableHeadCell: {
        root: {
           fontWeight:'bolder'
      }
      },
      MUIDataTableBodyRow: {
        root: {
          whiteSpace: 'nowrap',
          cursor:'pointer'
        }
      }, 

      MuiTableRow: {
        root: {
          '&$selected': {
            backgroundColor: 'yellow'
          }
        }
      },  

     
    }
  })





function Table (props){
  const router = useRouter()
  const {data , columns} = props;


  
  const handleRowClick = (rowData, rowMeta) => {
    router.push('/orders/'+rowData[0])
};

    
       return (
          <div className="card" style={{marginTop:'10px'}}>
               <MuiThemeProvider theme={getMuiTheme()}>
                  <MUIDataTable
                        title="Missing Orders"
                        data={data}
                        columns={columns}
                        options={{
                        filterType: "checkbox",
                        selectableRows: 'none',
                        onRowClick: handleRowClick,

                        }}
                        style={{boxShadow: "none"}}
                        
                        />
               </MuiThemeProvider>
          </div>  
          

        )
    }

export default Table


