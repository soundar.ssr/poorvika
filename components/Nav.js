import { Navbar, Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Image from "next/image";
import Link from "next/link";

function Homebar() {
  return (
    <>
      <Navbar
        style={{
          background: "#f7941e !important",
          borderBottom: "1px solid rgba(173, 173, 173, 0.2)",
          minHeight: "10vh",
        }}
      >
        <Container>
            <Link href="/" >
              <div style={{cursor:'pointer'}}>
                <Image
                  alt=""
                  src="/new-logo.png"
                  height={40}
                  width={100}
                  className="d-inline-block align-top"
                />{" "}
              </div>
            </Link>
           
        </Container>
      </Navbar>
    </>
  );
}

export default Homebar;
