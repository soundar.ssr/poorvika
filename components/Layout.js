import React from 'react';
import styles from '../styles/Home.module.css'
import Nav from './Nav';
import Sidebar from './Sidebar';

const Layout = ({children}) => {
    return (
        <>
        <Nav />
        <div className="col-md-12 row">
            <main className="col-md-12" style={{flex:1}}>
                {children}
            </main>
        </div>
        </>
    )
}

export default Layout
