const CartTop = ({color, value, title}) => {
    return(
        <div className="col-md-6 col-lg-3" style={{marginRight:'10px'}}>
        <div className="card report-card" style={{background:color, color:'white !important'}}>
            <div className="card-body">
                <div className="row d-flex justify-content-center">                                                
                    <div className="col">
                        <p className="mb-0 fw-semibold">{title}</p>
                        <h3 className="m-0">{value}</h3>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    )
    
}

export default CartTop;