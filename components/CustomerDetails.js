const OrderDetails = ({ order }) => {
  return (
    <div className="card row m-2">
      <div className="col-md-12 cart">
        <div className="title">
          <div className="row">
            <div className="col m-2">
              <h4>
                <b>Customer Details</b>
              </h4>
            </div>
          </div>
        </div>

        <div className="row border-top border-bottom">
          <div className="row main align-items-center m-2">
            <div>
              <strong>Name : </strong>
              {order.customer_details.customer}
            </div>
            <div>
              <strong>Phone : </strong>
              {order.customer_details.telephone}
            </div>
            <div>
              <strong>Email : </strong>
              {order.customer_details.email}
            </div>
          </div>
          <div className="row main align-items-center m-2">
            <div>
              <strong>Shipping Address </strong>
            </div>
            <div>{order.shipping_details.shipping_company}</div>
            <div>
              {order.shipping_details.shipping_address_1},{" "}
              {order.shipping_details.shipping_address_2}
            </div>
            <div>{order.shipping_details.shipping_area}</div>
            <div>{order.shipping_details.shipping_city}</div>
            <div>
              {order.shipping_details.shipping_country} -{" "}
              {order.shipping_details.shipping_postcode}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrderDetails;
