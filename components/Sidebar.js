import { ProSidebar, Menu, MenuItem, SubMenu, SidebarHeader } from 'react-pro-sidebar';
import 'react-pro-sidebar/dist/css/styles.css';

function Sidebar() {
  return(
    <ProSidebar>
      <Menu iconShape="square">
        <MenuItem >Dashboard</MenuItem>
        <SubMenu title="Missed Orders" >
          <MenuItem><a> New </a></MenuItem>
          <MenuItem>Pending</MenuItem>
          <MenuItem>Closed</MenuItem>
        </SubMenu>
      </Menu>
    </ProSidebar>
  )
}

export default Sidebar;

