import moment from "moment";
import Image from "next/image";

const StatusTable = ({ status = [] }) => {
  console.log(status)
  return (
    <div className="card row m-2">
          <div className="col-md-12 cart">
            <div className="title">
                <div className="row border-top border-bottom" style={{padding:'15px 0px'}}>
                  <div className="row main align-items-left m-2">
                    <div className="col-2"> <strong>Date</strong> </div>
                    <div className="col"> <strong>Status</strong> </div>
                    <div className="col"> <strong>Current Status</strong></div>
                    <div className="col"> <strong>Remarks </strong></div>
                    <div className="col"> <strong>Updated By</strong> </div>
                  </div>
                </div>
            </div>
            <div className="body">
                      {
                      status.map((item) => {
                        return (
                          <div key={item._id} className="row border-top border-bottom " style={{padding:'10px 0px'}} >
                          <div className="row main align-items-center m-2">
                            <div className="col-2"> {moment(item.updatedAt).format('DD-MM-YYYY hh:mm a' )} </div>
                            <div className="col"> {item.status} </div>
                            <div className="col">{item.current_status}</div>
                            <div className="col">{item.remarks} </div>
                            <div className="col">{item.updatedBy} </div>
                          </div>
                        </div>
                        );
                      })}
            </div>
          </div>
    </div>
  );
};

export default StatusTable;
