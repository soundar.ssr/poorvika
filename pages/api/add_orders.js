import {MongoClient} from 'mongodb';

export default async function handler(req, res) {
  const client = await MongoClient.connect('mongodb+srv://soundarssr:soundar@cluster0.cdyt2.mongodb.net/poorvika?retryWrites=true&w=majority')
  
  try {
    const db = client.db();
    const orderCollection = db.collection('orders');
    const ordersVal = req.body.ordersVal.map((item)=>{
                       return {...item, status:'Open', current_status: 'New' }
                           })
    const orders = await orderCollection.insertMany(ordersVal,{ordered: false});
    client.close();
    res.status(201).json({status:'success', success:true, message:'New Orders Added'})
  }catch(err){
    client.close();
    console.log(err)
    res.status(201).json({status:'error', success:false, message:err.message})
  }
  
}

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '100mb',
    },
  },
}
