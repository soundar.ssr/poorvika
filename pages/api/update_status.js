import { MongoClient } from "mongodb";

export default async function handler(req, res) {
  const { orderId, status, current, remarks, item_code, mobile } = req.body;
  console.log(item_code)

  try {
    const client = await MongoClient.connect(
      "mongodb+srv://soundarssr:soundar@cluster0.cdyt2.mongodb.net/poorvika?retryWrites=true&w=majority"
    );
    const db = client.db();
    const orderCollection = db.collection("orders");
    const statusCollection = db.collection("status");
    console.log(req.body);
    console.log(status);
    console.log(current);
    await orderCollection.updateMany(
      {
        "customer_details.telephone": mobile, 
        "product_details.item_code": item_code, 
         },
      { $set: { status: status, current_status: current,  } }
    );
    await statusCollection.insertOne({
      mobile:mobile,
      item_code:item_code,
      status:status,
      current_status:current,
      remarks:remarks,
      updatedBy:"Soundar",
      updatedAt:new Date()
   })
    client.close();
    res.status(201).json({
      status: "success",
      success: true,
      message: "Status Updated Successfully",
    });
  } catch (err) {
    console.log(err);
    res
      .status(201)
      .json({ status: "error", success: false, message: err.message });
  }
}
