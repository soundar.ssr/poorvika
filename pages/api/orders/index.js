import {MongoClient} from 'mongodb';

export default async function handler(req, res) {
  try{
    const client = await MongoClient.connect('mongodb+srv://soundarssr:soundar@cluster0.cdyt2.mongodb.net/poorvika?retryWrites=true&w=majority')
    const db = client.db();
    const orderCollection = db.collection('orders');
    const orders= await orderCollection.find().toArray();

    orderCollection.aggregate([
      {
        "$group": {
            "_id": {
              "mobile": "$customer_details.telephone",
              "products": "$product_details.item_code",
            },
         orders: { $push: "$$ROOT" } 

        }
      },
        {
          "$sort":  {
            "orders.date_added": 1,
            "_id.mobile": 1
          }
        }

    ]).toArray((err, filtered_orders)=>{
      client.close()
      res.status(201).json({status:'success',orders:filtered_orders, total_orders:orders});
      

    })
   
  }catch(err){
    console.log(err); 
    res.status(400).json({status:'error', message:err.message})
  }

}

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '1000mb',
    },
  },
}