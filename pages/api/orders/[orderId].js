import { MongoClient } from "mongodb";

export default async function handler(req, res) {
  try {
    const client = await MongoClient.connect(
      "mongodb+srv://soundarssr:soundar@cluster0.cdyt2.mongodb.net/poorvika?retryWrites=true&w=majority"
    );
    const db = client.db();
    const orderCollection = db.collection("orders");
    const statusCollection = db.collection("status");
    const order = await orderCollection
      .findOne({ order_id: Number(req.query.orderId) });
    const status = await statusCollection
      .find({ 
        mobile:order.customer_details.telephone,
        item_code:order.product_details[0].item_code
       })
      .toArray();  
    client.close();
    res.status(201).json({ status: "success", order: order, status: status });
  } catch (err) {
    console.log(err);
    res.status(400).json({ status: "error", message: err.message });
  }
}
