import { MongoClient } from "mongodb";
import axios from "axios";
import moment from "moment";

const getToken = async (db) => {
  try {
    const tokenCollection = db.collection("tokens");
    let token = await tokenCollection.find().toArray();
    let access_token = "";
    if (token.length === 0) {
      const result = await axios.get(
        "https://api.poorvika.com/auth/v1/auth/token"
      );
      access_token = result.data.data.access_token;
      const expired = moment().add("59", "seconds").valueOf();
      await tokenCollection.insertOne({
        token: access_token,
        expiredAt: expired,
      });
    } else if (token[0].expiredAt < moment().valueOf()) {
      const result = await axios.get(
        "https://api.poorvika.com/auth/v1/auth/token"
      );
      access_token = result.data.data.access_token;
      const expired = moment().add("59", "seconds").valueOf();
      await tokenCollection.updateOne({}, [
        { $set: { token: access_token, expiredAt: expired } },
      ]);
    } else access_token = token[0].token;
    return access_token;
  } catch (err) {
    console.log(err);
  }
};

export default async function handler(req, res) {
  try {
    const client = await MongoClient.connect(
      "mongodb+srv://soundarssr:soundar@cluster0.cdyt2.mongodb.net/poorvika?retryWrites=true&w=majority"
    );
    const db = client.db();
    const token = await getToken(db);
    client.close();
    res.status(201).json({ status: "success", success: true, token });
  } catch (err) {
    console.log(err);

    res
      .status(400)
      .json({ status: "error", success: false, message: err.message });
  }
}
