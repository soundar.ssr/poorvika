import axios from "axios";
import moment from "moment";

export default async function handler(req, res) {
  try {
    const token = await axios
      .get("https://ao.poorvika.com/api/auth")
      .catch((err) => {
        throw new Error(err.message);
      });

    axios.defaults.headers.common = {
      Authorization: `Bearer ${token.data.token}`,
    };
  
    const orders = await axios
    .post("https://api.poorvika.com/order/v1/oms/allOrders", {
      "from_date" : moment().startOf('day').format("YYYY-MM-DD"),
      "to_date"   : moment().endOf('day').format("YYYY-MM-DD")
    })
      .catch((err) => {
        throw new Error(err.message);
      });
    var ordersVal = orders.data.data;

    ordersVal = ordersVal.filter((item) => {
      if (item.product_details === null) return true;
      else if (
        item.product_details &&
        item.product_details[0].product_name != `test (don't use this)`
      )
        return true;
    });
    const mongo_result = axios
      .post("https://ao.poorvika.com/api/add_orders", { ordersVal })
      .catch((err) => {
        throw new Error(err.message);
      });
    const result = axios
      .post("https://ao.poorvika.com/api/add_customer", { ordersVal })
      .catch((err) => {
        throw new Error(err.message);
      });

    res.status(201).json({ message: "New Missed Order Added Successfully" });
  } catch (err) {
    console.log(err);
    res
      .status(400)
      .json({ status: "error", success: false, message: err.message });
  }
}

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '100mb',
    },
  },
}