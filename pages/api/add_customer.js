import pg  from 'pg';
var format = require('pg-format');

export default async function handler(req, res) {

  const customer_details = req.body.ordersVal.map((order, index)=> {
    
    return [order.customer_details.customer,
      order.customer_details.email,
      order.customer_details.telephone];
})
 try{
  var conString = "postgres://wkohgvvl:6J4IO1ue8fHZbYBQdgbwv_GJw6yNDhzW@chunee.db.elephantsql.com/wkohgvvl"
   var client = new pg.Client(conString);
   client.connect(function(err) {
     if(err) {
       return console.error('could not connect to postgres', err);
     }

     const sqlQuery = format('INSERT INTO customers (customer, email, mobile) VALUES %L', customer_details)+' ON CONFLICT DO NOTHING';
      client.query(sqlQuery, function(err, result) {
        if(err) {
          return console.error('error running query', err);
        }
        console.log('Total inserted rows : '+ result.rowCount);
        client.end()
      });
 
   });
  
  res.status(201).json({status:'success', success:true, message:'Customer Added'})
 }catch(err){
   console.log(err);
   res.status(400).json({status:'error', success:false, message:err.message})

 }

}

export const config = {
  api: {
    bodyParser: {
      sizeLimit: '100mb',
    },
  },
}
