import axios from "axios";
import ProductDetails from "../../components/ProductDetails";
import CustomerDetails from "../../components/CustomerDetails";
import PaymentDetails from "../../components/PaymentDetails";
import StatusDetails from "../../components/StatusDetails";
import StatusTable from "../../components/StatusTable";

const OrderDetailsPage = ({ order, currentVal, pending, closed, open, status }) => {
  
  return (
    <>
      <div className="col-md-12 row">
        <div className="col-md-7">
          <ProductDetails order={order} />
        </div>
        <div className="col-md-5">
          <StatusDetails
            order={order}
            currentVal={currentVal}
            open={open}
            pending={pending}
            closed={closed}
          />
        </div>
      </div>
      <div className="col-md-12 row">
        <div className="col-md-6">
          <CustomerDetails order={order} />
        </div>
        <div className="col-md-6">
          <PaymentDetails order={order} />
        </div>
      </div>
      <div className="col-md-12 row" style={{marginBottom:'100px'}}>
        <StatusTable  status={status}/>
        </div>

    </>
  );
};

export async function getServerSideProps(context) {
  try {
    const { orderId } = context.query;
    var order = await axios.get(
      "https://ao.poorvika.com/api/orders/" + orderId
    );
    var currentVal = [];
    const status = order.data.status;
    order = order.data.order;
    console.log(status)
    const pending = [
        {
          id: "Call me Later",
          val: "Call me Later",
        },
        {
          id: "Not Answered",
          val: "Not Answered",
        },
        {
          id: "Plan to buy",
          val: "Plan to buy",
        },
      ],
      closed = [
        {
          id: "Purchased At Poorvika",
          val: "Purchased At Poorvika",
        },
        {
          id: "Purchased At Others",
          val: "Purchased At Others",
        },
        {
          id: "Not Interested",
          val: "Not Interested",
        },
      ],
      open = [
        {
          id: "New",
          val: "New",
        },
      ];

    if (order.status === "Open") currentVal = open;
    else if (order.status === "Pending") currentVal = pending;
    else if (order.status === "Closed") currentVal = closed;

    return {
      props: {
        order: order,
        pending: pending,
        open: open,
        closed: closed,
        currentVal: currentVal,
        status
      },
    };
  } catch (err) {
    console.log(err);
    return {
      props: {
        order: [],
        pending: "",
        open: "",
        closed: "",
        currentVal: [],
        status:[]
      },
    };
  }
}

export default OrderDetailsPage;
